<?php

$datos = file_get_contents('php://input');

$obj = json_decode($datos,true);

$type = $obj['type'];
$event_date = $obj['event_date'];
$id = $obj['transaction']['id'];
$customer_id = $obj['transaction']['customer_id'];
$amount = $obj['transaction']['amount'];


$event_date = str_replace('T',' ',$event_date);
$event_date =  substr($event_date, 0, 19);
$event_date = $event_date.'.005';




ini_set("soap.wsdl_cache_enabled", "0");
try {

    $options = array('exceptions' => true, 'trace' => true);
    //->produccion
    //$cliente=new SoapClient("http://pageandroid.dyndns.org/WSeCommerceMX/WSeCommerceMX.asmx?wsdl", $options);
    //$ns="http://WSeCommerceMX.asmx/";

    //->Pruebs
    $cliente = new SoapClient("http://pageandroid.dyndns.org/WSecommdesarrollo/WSecommdesarrollo.asmx?wsdl", $options);
    $ns = "http://WSeCommdesarrollo.asmx/";

    $user = "e41b237f6ff928a5f22dfdb0f113a6f0";
    $headerbody = array('codigoent' => $user);
    $header = new SOAPHeader($ns, 'Acso', $headerbody);
    $cliente->__setSoapHeaders($header);
} catch (Exception $e) {
    include_once("err.php");
    http_response_code(400);
}

//public bool mcWebHookPost(string type, string event_date, double amount, string id, string clienteid)
if ($cliente) {
    $parameters = array(
        'type' => $type,
        'event_date' => $event_date,
        'amount' => $amount,
        'id' => $id,
        'clienteid' => $customer_id
        );
    $consulta  = $cliente->mcWebHookPost($parameters);
    $resultado = $consulta->mcWebHookPostResult;
    if ($resultado) {
        http_response_code(200);
    } else{
        http_response_code(500);
    }

}