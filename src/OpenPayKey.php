<?php


require_once '../vendor/autoload.php';


class OpenPayKey {

    // ge.avila@mavi.mx Keys
    private $MerchantID = 'movxmwvyy4coiids8uuk';
    private $Privatekey = 'sk_6724a8a945124fcc9453106030550ed1';
    private $PublicKey = 'pk_b4fcf61a6bc24bddaf79a6a24f5f4ee8';
    private $Production = false;
    //Sandbox Produccion
    //private $MerchantID = 'mzftoiaiybcuqkpuvao2';
    //private $Privatekey = 'sk_4253236ed03a4fb691184af457048f23';
    //private $PublicKey = 'pk_be08636aa1254d2fae543527e3094859';
    //private $Production = false;

    /**
     * @return string
     */
    private function getMerchantID () {
        return $this->MerchantID;
    }

    /**
     * @return string
     */
    private function getPrivatekey () {
        return $this->Privatekey;
    }

    /**
     * @return string
     */
    private function getPublicKey () {
        return $this->PublicKey;
    }


    /**
     * @return bool
     */
    private function isProduction () {
        return $this->Production;
    }

    /* Getters & Setters */

    private function conect () {
        Openpay::setId($this->getMerchantID());
        Openpay::setApiKey($this->getPrivatekey());

        Openpay::setProductionMode($this->isProduction());

        return Openpay::getInstance($this->getMerchantID(), $this->getPrivatekey());
    }

    private function setCustomerData ($name, $last_name, $email, $phone_number, $line1, $line2, $line3, $postal_code, $state, $city, $country_code) {

        $error = false;
        $MSGerror = 'Datos Erroneos';
        $customerData = array(
            'name' => $name,
            'last_name' => $last_name,
            'email' => $email,
            'phone_number' => $phone_number,
            'address' => array(
                'line1' => $line1,
                'line2' => $line2,
                'line3' => $line3,
                'postal_code' => $postal_code,
                'state' => $state,
                'city' => $city,
                'country_code' => $country_code
            )
        );


        $response = array(
          'code' => '214'
        , 'MSG' => 'Everything is fine'
        , 'data' => $customerData
        );

        if ($error) {
            $response['code'] = '442';
            $response['MSG'] = $MSGerror;
        }

        return $customerData;

    }

    public function sendCharge ($customerID,
                                $method, $amount
        , $name, $last_name, $email, $phone_number, $line1, $line2, $line3, $postal_code, $state, $city, $country_code) {

        $json = '';
        $ErrorMSG = '';

        $response = array(
              'code' => 214
            , 'MSG' => 'Everything is fine \\r\\n'
            , 'dataJson' => $json
        );

        try {

            if ((isset($method) and isset($amount)) && (($amount > 0.00 && $amount <= 29999.99))) {

                if (isset($customerID)) {
                    $charge = $this->newCustomerCharge($customerID, $method, $amount);
                    $response['MSG'] = $response['MSG'] . 'New Charge Created to: ' . $customerID . '\\r\\n';
                } else {
                    $customerData = $this->setCustomerData($name, $last_name, $email, $phone_number, $line1, $line2, $line3, $postal_code, $state, $city, $country_code);
                    $customer = $this->newCustomer($customerData);
                    $charge = $this->newCustomerCharge($customer->id, $method, $amount);
                    $response['MSG'] = $response['MSG'] . 'New Customer Charge Created \\r\\n';
                }

                $data = array(
                    'id' => $charge->id
                , 'customer_id' => $charge->customer_id
                , 'currency' => $charge->currency
                , 'transaction_type' => $charge->transaction_type
                , 'MerchantID' => $this->getMerchantID()
                , 'serializableData' => $charge->serializableData
                , 'pdfURL' => $this->getPDFurl($charge->serializableData['payment_method']->reference)
                );

                $myArray = json_decode(json_encode($data), true);
                $response['dataJson'] = $myArray;
            }

        } catch (OpenpayApiTransactionError $e) {
            error_log('ERROR on the transaction: ' . $e->getMessage() .
                ' [error code: ' . $e->getErrorCode() .
                ', error category: ' . $e->getCategory() .
                ', HTTP code: ' . $e->getHttpCode() .
                ', request ID: ' . $e->getRequestId() . ']', 0);
            $ErrorMSG = $ErrorMSG . 'ERROR on the transaction: ' . $e->getMessage() .
                ' [error code: ' . $e->getErrorCode() .
                ', error category: ' . $e->getCategory() .
                ', HTTP code: ' . $e->getHttpCode() .
                ', request ID: ' . $e->getRequestId() . '] \\r\\n';

            $ErrorMSG = $response['MSG'] . '\\r\\n' . $ErrorMSG;
            $response['code'] = 434;
            echo '<h1>1</h1>';
        } catch (OpenpayApiRequestError $e) {
            error_log('ERROR on the request: ' . $e->getMessage(), 0);
            $ErrorMSG = $ErrorMSG . 'ERROR on the request: ' . $e->getMessage() . '\\r\\n';
            $response['code'] = 434;
            echo '<h1>2</h1>';
        } catch (OpenpayApiConnectionError $e) {
            error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);
            $ErrorMSG = $ErrorMSG . 'ERROR while connecting to the API: ' . $e->getMessage() . '\\r\\n';
            $response['code'] = 434;
            echo '<h1>3</h1>';
        } catch (OpenpayApiAuthError $e) {
            error_log('ERROR on the authentication: ' . $e->getMessage(), 0);
            $ErrorMSG = $ErrorMSG . 'ERROR on the authentication: ' . $e->getMessage() . '\\r\\n';
            $response['code'] = 434;
            echo '<h1>4</h1>';
        } catch (OpenpayApiError $e) {
            error_log('ERROR on the API: ' . $e->getMessage(), 0);
            $ErrorMSG = $ErrorMSG . 'ERROR on the API: ' . $e->getMessage() . '\\r\\n';
            $response['code'] = 434;
            echo '<h1>5</h1>';
        } catch (Exception $e) {
            error_log('Error on the script: ' . $e->getMessage(), 0);
            $ErrorMSG = $ErrorMSG . 'Error on the script: ' . $e->getMessage() . '\\r\\n';
            $response['code'] = 434;
            echo '<h1>6</h1>';
        }

        if ($response['code'] == 434) {
            $response['MSG'] = $ErrorMSG;
        }


        var_dump(json_encode($response));
        return json_encode($response);
    }

    private function getCustomerInfo ($customerID) {
        $openpay = $this->conect();
        return $openpay->customers->get($customerID);
    }

    private function newCustomerCharge ($customerID, $method, $amount) {
        $openpay = $this->conect();
        $chargeData = array(
            'method' => $method,
            'amount' => 11.02,
            'description' => 'Cargo a tienda');
        $customer = $openpay->customers->get($customerID);
        return $customer->charges->create($chargeData);
    }

    private function newCustomer ($customerData) {
        $openpay = $this->conect();
        return $openpay->customers->add($customerData);
    }

    private function getPDFurl ($reference) {
        if (!$this->isProduction()) {
            return 'https://sandbox-dashboard.openpay.mx/paynet-pdf/' . $this->getMerchantID() . '/' . $reference;
        } else {
            return 'https://dashboard.openpay.mx/paynet-pdf/' . $this->getMerchantID() . '/' . $reference;
        }
    }


}