CREATE TABLE CXCCMensajeWebHookOpenPay (
    ClienteOpenPay VARCHAR(30)
  , CargoOpenPay   VARCHAR(30)
  , Tipo           VARCHAR(20)
  , Importe        MONEY
  , Fecha          DATETIME
  , Cobro          BIT
  , IdCxc          INT NULL
)

ALTER TABLE CXCCMensajeWebHookOpenPay
ADD CONSTRAINT DF_CXCCMensajeWebHookOpenPay_Cobro
DEFAULT 0 FOR Cobro

EXEC SpAgregarDescripcionTabla @nombreTabla = N'CXCCMensajeWebHookOpenPay',
                               @descripcion = N'Tabla que guarda los mensajes del WebHook de OpenPay';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'ClienteOpenPay',
                                          @descripcion   = N'Numero de cliente de OpenPay';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'CargoOpenPay',
                                          @descripcion   = N'Numero de cargo del OpenPay';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'Tipo',
                                          @descripcion   = N'Tipo del mensaje';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'Importe',
                                          @descripcion   = N'importe del mensaje';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'Fecha',
                                          @descripcion   = N'Fecha del mensaje';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'Cobro',
                                          @descripcion   = N'Bandera que registra el cobro 1 = registrado, 0 =  sin registrar ';

EXEC SpAgregarDescripcionColumnaPorTablas @nombresTablas = N'CXCCMensajeWebHookOpenPay',
                                          @nombreColumna = N'IdCxc',
                                          @descripcion   = N'id de Cxc en caso de que se genere un movimiento de Cobranza';

