SET DATEFIRST 7
SET ANSI_NULLS ON
SET ANSI_WARNINGS ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET LOCK_TIMEOUT - 1
SET QUOTED_IDENTIFIER OFF
SET NOCOUNT ON
SET IMPLICIT_TRANSACTIONS OFF
GO
--======================================================================================================================
--NOMBRE         : SPCXCWebHookMensajeOpenPay
--AUTOR          : Getsemani Avila Quezada ༼ つ ◕_◕ ༽つ
--FECHA CREACIÓN : 29/04/2020
--DESARROLLO     : ACW00072 WebHook Openpay
--MODULO         : CXC
--DESCRIPCIÓN    : SP para Insertar mensajes desde web los mensajes del WebHook
--EJEMPLO        : EXEC SPCXCWebHookMensajeOpenPay '','','',1,'2020-04-29 13:27:26.000'
--======================================================================================================================
CREATE PROCEDURE SPCXCWebHookMensajeOpenPay @clienteOpenPay varchar(30)-- Variable del Json: clienteOpenPay
, @cargoOpenPay varchar(30)                                            -- Variable del Json: transaction.id
, @tipo varchar(20)                                                    -- Variable del Json: type
, @importe money                                                       -- Variable del Json: transaction.amount
, @fecha datetime                                                      -- Variable del Json: event_date
AS
BEGIN
  INSERT INTO CXCCMensajeWebHookOpenPay (clienteOpenPay, cargoOpenPay, tipo, importe, fecha)
    VALUES (@clienteOpenPay, @cargoOpenPay, @tipo, @importe, @fecha)
END